﻿namespace FacadePattern
{
    // Данный класс является Фасадом для создания мотиватора
    public class Motivator
    {
        private PicturesHandler picturesHandler;

        private TextMotivator textMotivator;

        private BackgroundMotivator backgroundMotivator;

        private ModernMotivator modernMotivator;        

        public Motivator()
        {
            Initialization();
        }

        private void Initialization()
        {
            picturesHandler = new PicturesHandler();

            textMotivator = new TextMotivator();

            backgroundMotivator = new BackgroundMotivator();

            modernMotivator = new ModernMotivator();
        }

        public string Create(string path, string text)
        {     
            picturesHandler.Resize(path);

            var formattedText = textMotivator.FontCreation(text);

            var drawingVisual = backgroundMotivator.Creature(formattedText);

            string nameFile = modernMotivator.ImageCreation(textMotivator, backgroundMotivator, drawingVisual);

            return nameFile;
        }
    }
}
