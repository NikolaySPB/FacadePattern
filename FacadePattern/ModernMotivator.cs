﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FacadePattern
{
    // Данный класс компонует и создает мотиватор
    public class ModernMotivator
    {
        private readonly string name = "output.jpg";

        public string ImageCreation(TextMotivator textMotivator, BackgroundMotivator backgroundMotivator, DrawingVisual drawingVisual)
        {
            var bmp = new RenderTargetBitmap(backgroundMotivator.totalWidth, backgroundMotivator.totalHeight, textMotivator.dpi, textMotivator.dpi, PixelFormats.Pbgra32);

            bmp.Render(drawingVisual);

            var encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bmp));

            using (var stream = File.Create(Program.Path + name))
            {
                encoder.Save(stream);
            }

            return name;
        }
    }
}
