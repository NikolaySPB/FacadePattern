﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FacadePattern
{
    // Данный класс подготавливает фон, рамку будущего мотиватора
    public class BackgroundMotivator
    {
        private DrawingVisual drawingVisual;

        public int totalWidth;

        public int totalHeight;

        private const int gap = 20;

        public DrawingVisual Creature( FormattedText formattedText)
        {
            var image = BitmapFrame.Create(new Uri(PicturesHandler.TemporaryFilePath));

            totalWidth = (int)Math.Ceiling((double)image.PixelWidth + 2 * gap);

            totalHeight = (int)Math.Ceiling((double)image.PixelHeight + 3 * gap + formattedText.Height);

            drawingVisual = new DrawingVisual();

            using (var drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawRectangle(Brushes.DeepSkyBlue, null, new Rect(0, 0, totalWidth, totalHeight));

                drawingContext.DrawImage(image, new Rect(gap, gap, image.PixelWidth, image.PixelHeight));

                drawingContext.DrawText(formattedText, new Point(gap, image.PixelWidth + 2 * gap));
            }
            return drawingVisual;
        }
    }
}
