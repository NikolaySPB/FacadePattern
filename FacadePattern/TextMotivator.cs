﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FacadePattern
{
    // Данный класс работает с текстом будущего мотиватора
    public class TextMotivator
    {
        private readonly int fontSize = 20;

        public readonly int dpi = 96;

        public FormattedText FontCreation(string text)
        {
            var font = new Typeface(new FontFamily("Segoe UI"), FontStyles.Normal, FontWeights.Bold, FontStretches.SemiExpanded);

            var image = BitmapFrame.Create(new Uri(PicturesHandler.TemporaryFilePath));

            var imageWidth = image.PixelWidth;

            var formattedText = new FormattedText(text, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, font, fontSize, Brushes.White, dpi)
            {
                MaxTextWidth = imageWidth,

                TextAlignment = TextAlignment.Center
            };

            return formattedText;
        }
    }
}
