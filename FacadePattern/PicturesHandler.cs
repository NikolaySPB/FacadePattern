﻿using System.Drawing;

namespace FacadePattern
{
    //Данный класс сжимает картинку под размер 400х400 пикселей и сохраняет во временный файл
    public class PicturesHandler
    {
        private const int size = 400;

        public static string TemporaryFilePath = @"\temporaryFile.png";
       
        public void Resize(string path)
        {
            Bitmap bitmap = new Bitmap(new Bitmap(path), new Size(size, size));

            bitmap.Save(Program.Path + TemporaryFilePath);
        }
    }
}
