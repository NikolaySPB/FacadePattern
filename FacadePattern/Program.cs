﻿using System;

namespace FacadePattern
{
    class Program
    {
        public static string Path = AppDomain.CurrentDomain.BaseDirectory;
        static void Main(string[] args)
        {
            string imageDolphinsPath = Path + @"\Дельфины.jpg";

            string imageTigerPath = Path + @"\Тигр.jpg";

            string imageNaturePath = Path + @"\Природа.jpg";

            string text = "Новый мотиватор - это хорошо!!!";

            Motivator motivator = new Motivator();

            string nameNewFile = motivator.Create(imageDolphinsPath, text);

            Console.WriteLine(nameNewFile);

            Console.ReadLine();
        }
    }
}
